function set_paths(base_dir_code)
    addpath(fullfile(base_dir_code, 'code_matlab'));
    addpath(fullfile(base_dir_code, 'code_matlab/rnnFramework'));
end

